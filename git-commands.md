# GIT commands
v2018.09.29 at 20h35

## courantes

`$ git checkout -b it03 origin/it03`  
Branch it03 set up to track remote branch it03 from origin.  
Switched to a new branch 'it03'  
**_rien a voir_** avec `$ git checkout -b origin/it02` à partir
 de la branche **_local courante_** créer une nouvelle branche local et pointer dessus  


`$ git fetch --prune`  
rapatrie toutes les branches distantes y compris les updates et autres;
permet également d'être au courant lorsqu'une nouvelle branche a été créer dans bitbucket. 
`--prune` before fetching, remove any remote-tracking references that no longer exist on the remote. 
si plusieurs repositories alors `$ git fetch --all --prune`  





`$ git pull`  
mettre à jour le dépot local (se mettre au préalabe sur la branche voulue).
`git fetch` + `git merge origin/[une-branche]` = `git pull`


`$ git status`  
donner le status de la branche courante  
p.ex. : Your branch is behind 'origin/it03' by 6 commits, and can be fast-forwarded.  
**toujours** faire un `git fetch` avant un `git status`  

`$ git branch` (local)  
`$ git branch -r` (remote)  
`$ git branch -v -a` (verbose all)  
`$ git branch -d it01` (delete local branch)  
git branch, mieux vaut toujours utiliser -v pour savoir l'état de nos branches (behind)


`$ git checkout [ma-branche-en-local]`  
se déplacer

`$ git add git-commands.md`  
ajouter un fichier au futur commit

`$ git commit -m "git-commands.md v2018.09.28"`  
commiter les modifications en local

`λ git push origin it04`  
pushed a branch called `it04` to a shared repository `origin`



## liens web
git - petit guide : <http://rogerdudler.github.io/git-guide/index.fr.html>  
Learn Git Branching : <https://learngitbranching.js.org>  
Learn Git Version Control : <https://www.katacoda.com/courses/git/1>  

Basic vi Commands : <https://www.cs.colostate.edu/helpdocs/vi.html>  

Markdown Cheatsheet : <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>

## initialisation

1) GIT config  
`git config -l`  
`git config --list`

2) Create a new local repository  
`git init`

3) reprendre un projet (à faire dans le workspace)  
U80801076@CM016828 MINGW32 /c/ejpd/workspacesIJ  
`$ git clone http://DAH@intra.zas.admin.ch/bitbucket/scm/sed/sedex-batchs.git`  
`$ git clone https://peng-ouille@bitbucket.org/peng-ouille/perso.git`

4) si problème de **_login à rentrer_** tout le temps, faire  
U80801076@CM016828 MINGW32 /c/ejpd/workspacesIJ/sedex-batchs (develop)  
`$ git config --global credential.helper "manager"`  
cette config est visible par `$ git config –-list`  
ce compte git est visible dans Windows sous  
`Panneau de configuration\Comptes d’utilisateurs\Gestionnaire d'identification`  
voir aussi `Panneau de configuration\Tous les Panneaux de configuration`,
 `Options Internet`, onglet `Connexions` boutons `Paramètres` et  `Paramètres réseau`

